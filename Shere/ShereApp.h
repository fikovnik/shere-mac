//
//  ShereApp.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2017-11-16
//

#import <Cocoa/Cocoa.h>

/**
 * Entry application class
 * This class initializes fundamental components and interlinks them
 */
@interface ShereApp : NSObject <NSApplicationDelegate>

/**
 * Called when clicked on the Dock app icon
 */
- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)windowsVisible;

/**
 * Run Shere
 */
- (void) run;

@end

