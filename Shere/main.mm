//
//  main.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2017-11-06
//

#import <Cocoa/Cocoa.h>
#import "ShereApp.h"

int main(int argc, const char * argv[])
{
    ShereApp * shere = [[ShereApp alloc] init];
    [shere run];
    return 0;
}
