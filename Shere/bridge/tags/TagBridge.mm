//
//  TagTreeBridge.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-10
//

#import "TagBridge.h"
#import "TagTreeDataSource.h"
#import "UINoteHeader.h"
#import <set>

using Shere::NoteInfo;
using std::set;

@interface TagBridge ()
{
    shared_ptr<Core> core;
    UserInterface * ui;
    TagTreeDataSource * tagTreeDataSource;
    
    // Tag path that is actually selected. There can be 3 states:
    // - select a specific tag path (the tag path is non-empty)
    // - select all notes (the tag path is empty: @"")
    // - select untagged notes (the tag path is nil)
    NSString * selectedTagPath;
}

@end

@implementation TagBridge


//
// Actions
//


// Set new tag tree
- (void) setTagTree:(shared_ptr<iTagTree>) tagTree_
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->tagTreeDataSource setTagTree:tagTree_];
        [self->ui.sidebar reloadTags];
        [self selectTagPath:self->selectedTagPath expand:NO];
    });
}


//
// Other
//


- (TagBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_
{
    if(!(self = [super init])) return self;
    core = core_;
    ui = ui_;
    selectedTagPath = nil;
    tagTreeDataSource = [[TagTreeDataSource alloc] init];
    [ui.sidebar setDataSource:tagTreeDataSource];
    
    [self linkCoreWithUi];
    return self;
}


// Link events that flow between the user interface and the Core
- (void) linkCoreWithUi
{
    // On tag selected from sidebar
    ui.sidebar.onTagSelected = ^(NSString * tagPath)
    {
        self->selectedTagPath = tagPath;
        [self->ui.sidebar setUntaggedHighlighted:NO];
        [self selectTagPath:self->selectedTagPath expand:NO];
        
        auto tagTree = [self->tagTreeDataSource getTagTree];
        auto notes = tagTree->notesByTag([tagPath UTF8String]);
        [self setNoteList:notes];
    };
    
    // Show untagged notes
    ui.sidebar.onShowUntaggedClicked = ^
    {
        self->selectedTagPath = nil;
        [self->ui.sidebar deselectAllTags];
        [self->ui.sidebar setUntaggedHighlighted:YES];
        
        auto tagTree = [self->tagTreeDataSource getTagTree];
        auto notes = tagTree->notesWithoutTag();
        [self setNoteList:notes];
    };
    
    // On tag selected from editor
    ui.editor.onTagClicked = ^(NSString * _Nonnull tagPath)
    {
        self->selectedTagPath = tagPath;
        [self->ui.sidebar setUntaggedHighlighted:NO];
        [self selectTagPath:self->selectedTagPath expand:YES];
    };

    // Tag tree was refreshed
    core->notes.onTagTreeChanged = [self](shared_ptr<iTagTree> tagTree_)
    {
        [self setTagTree:tagTree_];
        [self reloadNoteList:tagTree_];
    };
    
    // Reload notes that where shown before search
    ui.sidebar.onSearchEnd = ^
    {
        auto tagTree = [self->tagTreeDataSource getTagTree];
        [self reloadNoteList:tagTree];
    };
    
    // Perform search
    ui.sidebar.onSearch = ^(NSString * searchQuery)
    {
        if([searchQuery length] == 0){
            auto tagTree = [self->tagTreeDataSource getTagTree];
            [self reloadNoteList:tagTree];
            return;
        }
        
        string query = [searchQuery UTF8String];
        self->core->notes.search(query);
    };
    
    // Search result
    core->notes.onSearchResult = [self](set<shared_ptr<NoteInfo>> foundNotes)
    {
        [self setNoteList:foundNotes];
    };
}


// Convert std::set of NoteInfo pointers to NSArray * of UINoteHeaders
- (void) setNoteList:(set<shared_ptr<NoteInfo>>) notes
{
    NSArray * notesObjc = [self noteSetToArray:notes];
    [ui.noteList setNotes:notesObjc];
}


// Select tag path in sidebar. If the parameter 'expand' is true,
// the outline view will be expanded to show the tag path.
- (void) selectTagPath:(NSString *) tagPath expand:(BOOL)expand
{
    if(tagPath == nil)
        return;

    // When actually deleted tag is selected, unselect it and show notes
    shared_ptr<iTagTree> tagTree = [tagTreeDataSource getTagTree];
    if(!tagTree->hasTag([tagPath UTF8String])){
        tagPath = @"";
        [ui.sidebar deselectAllTags];
    };

    if(expand)
        [self expandTagPath:tagPath];
}


// Expand tag path in sidebar's tag outline
- (void) expandTagPath:(NSString *) tagPath
{
    NSArray * tags = [tagTreeDataSource cachedTagComponents:tagPath];
    for(NSString * tag in tags)
    {
        NSString * cachedTag = [tagTreeDataSource getCachedTag:tag];
        [ui.sidebar.outlineView expandItem:cachedTag];
    }
        
    NSUInteger row = [ui.sidebar.outlineView rowForItem:[tags lastObject]];
    [ui.sidebar.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
}


// Convert notes in std::set to NSArray
- (NSArray *) noteSetToArray:(const set<shared_ptr<NoteInfo>> &) noteSet
{
    auto noteArray = [[NSMutableArray alloc] initWithCapacity:noteSet.size()];
    for(auto & note : noteSet)
    {
        auto noteId = [NSString stringWithUTF8String:note->noteId.c_str()];
        auto title = [NSString stringWithUTF8String:note->title.c_str()];
        auto noteHeader = [[UINoteHeader alloc] initWithId:noteId title:title];
        [noteArray addObject:noteHeader];
    }
    
    return noteArray;
}

//TODO: remove tag tree argument and get from tag tree data source
- (void) reloadNoteList:(shared_ptr<iTagTree>) tagTree
{
    set<shared_ptr<NoteInfo>> notes;
    if(selectedTagPath != nil)
        notes = tagTree->notesByTag([selectedTagPath UTF8String]);
    else
        notes = tagTree->notesWithoutTag();
    [self setNoteList:notes];
}

@end
