//
//  TagBridge.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-10
//

#import <Foundation/Foundation.h>
#import "UserInterface.h"
#import <Shere/Core.h>
#import <memory>

using Shere::Core;
using Shere::iTagTree;
using std::shared_ptr;

/**
 * Bridge between tag tree manipulation in Core and in UI.
 */
@interface TagBridge : NSObject

- (TagBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_;

//
// Actions
//

- (void) setTagTree:(shared_ptr<iTagTree>) tagTree;

@end

