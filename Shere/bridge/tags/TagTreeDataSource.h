//
//  TagTreeDataSource.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-21
//

#import <Cocoa/Cocoa.h>
#import <vector>
#import <string>
#import <memory>
#import <Shere/iTagTree.hpp>

using std::vector;
using std::string;
using std::shared_ptr;
using Shere::iTagTree;

/**
 * This class implements a bridge between Core's TagTree class and NSOutlineView
 * using NSOutlineViewDataSource protocol. In order to keep the expanded/collapsed
 * state of the outline, a simple pointer cache is used to keep the pointers
 * the same, if possible.
 */
@interface TagTreeDataSource : NSObject <NSOutlineViewDataSource>
{
    // Sef of NSStrings. This creates a 'model' for outline view.
    // If there is a new tag tree with the same tags, the NSString
    // pointers remain the same in order to prevent the outline view
    // to reset its collapsed / expanded state.
    NSMutableSet * tagCache;
    
}
/**
 * Set tags
 */
- (void) setTagTree:(shared_ptr<iTagTree>) tagTree;

/**
 * Get tags
 */
- (shared_ptr<iTagTree>) getTagTree;

/**
 * Get cached NSString* components (subtag hierarchy). Used for NSOutlineView item expanding.
 * For tag hi/my/bro returns these components (tag paths):
 * hi
 * hi/my
 * hi/my/bro
 */
- (NSArray *) cachedTagComponents:(NSString *) tagPath;

/**
 * Get cached tag path object.
 */
- (NSString *) getCachedTag:(NSString * ) tagPath;

/**
 * Get cached tag path object. If the tag path does not exist,
 * it will be created new one.
 */
- (NSString *) getOrCreateCachedTag:(NSString * ) tagPath;


//
// NSOutlineViewDataSource protocol implementation
//

- (NSInteger) outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item;
- (BOOL) outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item;
- (id) outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item;
- (id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item;

@end
