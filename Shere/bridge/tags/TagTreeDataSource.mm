//
//  TagTreeDataSource.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-21
//

#import "TagTreeDataSource.h"

@interface TagTreeDataSource ()
{
    shared_ptr<iTagTree> tagTree;
}

@end

@implementation TagTreeDataSource


- (id) init
{
    if(!(self = [super init])) return self;
    tagCache = [[NSMutableSet alloc] init];
    return self;
}


- (void) setTagTree:(shared_ptr<iTagTree>) tagTree_
{
    
    // Clean cache - remove non existing tags
    // The rest of the cache (new tags) is cached lazily
    
    auto tagsToRemove = [NSMutableSet new];
    for(NSString * tag in tagCache)
        if(!tagTree_->hasTag([tag UTF8String]))
            [tagsToRemove addObject:tag];
    [tagCache minusSet:tagsToRemove];
    tagTree = tagTree_;
}


- (shared_ptr<iTagTree>) getTagTree
{
    return tagTree;
}


- (NSArray *) cachedTagComponents:(NSString *) tagPath
{
    NSArray * tags = [tagPath componentsSeparatedByString:@"/"];
    NSMutableArray * components = [[NSMutableArray alloc] initWithCapacity:[tags count]];
    NSMutableString * tagPathPart = [NSMutableString stringWithString:[tags firstObject]];
    
    // First part
    [components addObject:[tagPathPart copy]];
    
    // Other parts
    for(int i=1; i < [tags count]; ++i)
    {
        [tagPathPart appendString:@"/"];
        [tagPathPart appendString:[tags objectAtIndex:i]];
        [components addObject:[tagPathPart copy]];
    }
    return components;
}


- (NSString *) getCachedTag:(NSString * ) tagPath
{
    return [tagCache member:tagPath];
}


- (NSString *) getOrCreateCachedTag:(NSString * ) tagPath
{
    NSString * cachedTag = [tagCache member:tagPath];
    if(cachedTag != nil)
        return cachedTag;
    [tagCache addObject:tagPath];
    return tagPath;
}

//
// NSOutlineViewDataSource
//


- (NSInteger) outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    if(tagTree == nullptr)
        return 0;
    
    string tagPath;
    if(item != nil)
        tagPath = [(NSString *)item UTF8String];
    
    auto subtagCount = tagTree->numberOfSubtags(tagPath);
    return subtagCount;
}


- (BOOL) outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    NSInteger childrenCount = [self outlineView:outlineView numberOfChildrenOfItem:item];
    return childrenCount > 0;
}


- (id) outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
    string tagPath;
    if(item != nil)
        tagPath = [(NSString*)item UTF8String];
    
    string subtagPath = tagTree->subtagAtIndex(tagPath, (int)index);
    NSString * subtagObjc = [NSString stringWithUTF8String:subtagPath.c_str()];
    NSString * cachedTag = [self getOrCreateCachedTag:subtagObjc];
    return cachedTag;
}


- (id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
    // Show only the last subtag: in 'hi/bro/how' show only 'how'
    NSString * tagPath = item;
    NSArray * hierarchy = [tagPath componentsSeparatedByString:@"/"];
    return [hierarchy lastObject];
}


@end
