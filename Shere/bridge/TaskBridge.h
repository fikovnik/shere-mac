//
//  TaskBridge.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-27
//

#import "UserInterface.h"
#import <Foundation/Foundation.h>
#import <Shere/Core.h>
#import <memory>

using Shere::Core;
using std::shared_ptr;

/**
 * Bridge between Task manipulation in Core and in UI.
 */
@interface TaskBridge : NSObject

- (TaskBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_;


//
// Actions
//


/**
 * Open task overview in editor
 */
- (void) openTasks:(const string &) tasksText;


@end

