//
//  NoteBridge.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-02
//

#import <Foundation/Foundation.h>
#import "UserInterface.h"
#import <Shere/Core.h>
#import <memory>

using Shere::Core;
using std::shared_ptr;

/**
 * Bridge between single-note manipulation in Core and in UI.
 */
@interface NoteBridge : NSObject

- (NoteBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_;

@end
