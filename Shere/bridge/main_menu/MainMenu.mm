//
//  MainMenu.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import "MainMenu.h"
#import <Sparkle/Sparkle.h>

// Pause undeclared selector warnings here in main menu, because the state of
// a menu item (enabled / disabled) is computed based on if the target impleents
// this selector. So make sure the selector is not mistyped!
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"


// Private methods
@interface MainMenu ()
{
    shared_ptr<Core> core;
    UserInterface * ui;
}

//
// Internal bridge funtions between views and Shere::Core methods (target on
// NSButton must be objective-c function, not C++ function.
//

- (void) actionCreateNote;


@end

@implementation MainMenu

- (MainMenu *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_
{
    if(!(self = [super init])) return self;
    core = core_;
    ui = ui_;
    
    // Create menus help: https://stackoverflow.com/q/9155015
    //
    // Note: each menubar button like "File", "Edit" has two parts: NSMenu
    // and NSMenuItem. This is how a new menubar button with one menu item is
    // added:
    //
    // NSMenuItem * menubarItem = [[NSMenuItem alloc] init];
    // NSMenu * newMenu = [[NSMenu alloc] initWithTitle:@"File"];
    // [menubarItem setSubmenu:newMenu];
    // [menubar addItem:newItem];
    //
    // NSMenuItem * menuItem = [[NSMenuItem alloc] init];
    // [menuItem setTitle:@"Open..."];
    // ... set target, key equivalent, ...
    // [newMenu addItem:menuItem];
    
    // Empty menu bar.
    NSMenu * menubar = [NSMenu new];
    [NSApp setMainMenu: menubar];
    
    [self initShereMenu];
    [self initNoteMenu];
    [self initEditMenu];
    [self initToolsMenu];
    [self initDebugMenu];
    
    return self;
}


- (void) initShereMenu
{
    NSMenuItem * shereMenuItem = [[NSMenuItem alloc] init];
    [[NSApp mainMenu] addItem:shereMenuItem]; // order matters - adding two lines below, the menubar shows two apple icons... :/
    NSMenu * shereMenu = [[NSMenu alloc] initWithTitle:@"Shere"];
    [shereMenuItem setSubmenu:shereMenu];
    
    // Check for updates
    NSMenuItem * updateItem = [[NSMenuItem alloc] init];
    [updateItem setTitle:@"Check For Updates"];
    [updateItem setAction:@selector(checkForUpdates:)];
    [updateItem setTarget:[SUUpdater sharedUpdater]];
    [shereMenu addItem:updateItem];
    
    // Separator in between
    NSMenuItem * separatorMenuItem = [NSMenuItem separatorItem];
    [shereMenu addItem:separatorMenuItem];
    
    // Quit Shere
    NSString * quitTitle = @"Quit Shere";
    NSMenuItem * quitItem = [[NSMenuItem alloc] init];
    [quitItem setTitle:quitTitle];
    [quitItem setAction:@selector(terminate:)];
    [quitItem setKeyEquivalent:@"q"];
    [shereMenu addItem:quitItem];
}


- (void) initNoteMenu
{
    NSMenuItem * noteItem = [[NSMenuItem alloc] init];
    NSMenu * noteMenu = [[NSMenu alloc] initWithTitle:@"Note"];
    [noteItem setSubmenu:noteMenu];
    [[NSApp mainMenu] addItem:noteItem];
    
    // New note
    NSString * newNoteTitle = @"New Note";
    NSMenuItem * newNoteItem = [[NSMenuItem alloc] init];
    [newNoteItem setTitle:newNoteTitle];
    [newNoteItem setAction:@selector(actionCreateNote)];
    [newNoteItem setTarget: self];
    [newNoteItem setKeyEquivalent:@"n"];
    [noteMenu addItem:newNoteItem];
}


- (void) initEditMenu
{
    NSMenuItem * editItem = [[NSMenuItem alloc] init];
    NSMenu * editMenu = [[NSMenu alloc] initWithTitle:@"Edit"];
    [editItem setSubmenu:editMenu];
    [[NSApp mainMenu] addItem:editItem];
    
    // Undo
    NSMenuItem * undo = [[NSMenuItem alloc] init];
    [undo setTitle:@"Undo"];
    //[undo setAction:@selector(undo:)]; // works in search and web view
    [undo setKeyEquivalent:@"z"];
    [editMenu addItem:undo];
    
    // Redo
    NSMenuItem * redo = [[NSMenuItem alloc] init];
    [redo setTitle:@"Redo"];
    //[redo setAction:@selector(redo:)]; // works in search and web view
    [redo setKeyEquivalent:@"z"];
    [redo setKeyEquivalentModifierMask: (NSEventModifierFlagShift | NSEventModifierFlagCommand)];
    [editMenu addItem:redo];
    
    // Separator
    NSMenuItem * separator = [NSMenuItem separatorItem];
    [editMenu addItem:separator];
    
    // Cut
    NSMenuItem * cut = [[NSMenuItem alloc] init];
    [cut setTitle:@"Cut"];
    [cut setAction:@selector(cut:)];
    [cut setKeyEquivalent:@"x"];
    [editMenu addItem:cut];
    
    // Copy
    NSMenuItem * copy = [[NSMenuItem alloc] init];
    [copy setTitle:@"Copy"];
    [copy setAction:@selector(copy:)];
    [copy setKeyEquivalent:@"c"];
    [editMenu addItem:copy];
    
    // Paste
    NSMenuItem * paste = [[NSMenuItem alloc] init];
    [paste setTitle:@"Paste"];
    [paste setAction:@selector(paste:)];
    [paste setKeyEquivalent:@"v"];
    [editMenu addItem:paste];
    
    // Select all
    NSMenuItem * selectAll = [[NSMenuItem alloc] init];
    [selectAll setTitle:@"Select All"];
    [selectAll setAction:@selector(selectAll:)];
    [selectAll setKeyEquivalent:@"a"];
    [editMenu addItem:selectAll];
    
    // Separator
    separator = [NSMenuItem separatorItem];
    [editMenu addItem:separator];
    
    // Find
    NSMenuItem * find = [[NSMenuItem alloc] init];
    [find setTitle:@"Find"];
    [find setAction:@selector(actionFind)];
    [find setTarget:self];
    [find setKeyEquivalent:@"f"];
    [editMenu addItem:find];
}


- (void) initToolsMenu
{
    NSMenuItem * toolsItem = [[NSMenuItem alloc] init];
    NSMenu * toolsMenu = [[NSMenu alloc] initWithTitle:@"Tools"];
    [toolsItem setSubmenu:toolsMenu];
    [[NSApp mainMenu] addItem:toolsItem];

    // Show all tasks
    NSMenuItem * tasksText = [[NSMenuItem alloc] init];
    [tasksText setTitle:@"Tasks"];
    [tasksText setAction:@selector(actionOpenTasks)];
    [tasksText setTarget:self];
    [tasksText setKeyEquivalentModifierMask: (NSEventModifierFlagShift | NSEventModifierFlagCommand)];
    [tasksText setKeyEquivalent:@"t"];
    [toolsMenu addItem:tasksText];
}


- (void) initDebugMenu
{
    NSMenuItem * debugItem = [[NSMenuItem alloc] init];
    NSMenu * debugMenu = [[NSMenu alloc] initWithTitle:@"Debug"];
    [debugItem setSubmenu:debugMenu];
    [[NSApp mainMenu] addItem:debugItem];

    // Paste debug note text
    NSMenuItem * debugText = [[NSMenuItem alloc] init];
    [debugText setTitle:@"Paste debug text"];
    [debugText setAction:@selector(pasteDebugText)];
    [debugMenu addItem:debugText];
    
    // Show all hidable tokens
    NSMenuItem * showHidable = [[NSMenuItem alloc] init];
    [showHidable setTitle:@"Show hidable tokens"];
    [showHidable setAction:@selector(showAllHidable)];
    [showHidable setKeyEquivalentModifierMask: (NSEventModifierFlagShift | NSEventModifierFlagCommand)];
    [showHidable setKeyEquivalent:@"s"];
    [debugMenu addItem:showHidable];
}


//
// Core and UI bridge functions
//

- (void) actionCreateNote
{
    core->notes.createNote();
}

- (void) actionOpenTasks
{
    core->tasks.loadTasks();
}

- (void) actionFind
{
    [ui.sidebar focusSearchField];
}

@end

// Turn on disabled warning
#pragma clang diagnostic pop
