//
//  MainMenu.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import <Cocoa/Cocoa.h>
#import <Shere/Core.h>
#import "UserInterface.h"

using Shere::Core;
using std::shared_ptr;

/**
 * Main menu contains almost all possible commands that can be fired within the App.
 * The enabled/disabled state of the buttons is determined using the responder
 * chain.
 */
@interface MainMenu : NSObject

- (MainMenu *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_;

@end
