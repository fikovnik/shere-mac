//
//  NoteBridge.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-02
//

#import "NoteBridge.h"

using Shere::NoteInfo;

@interface NoteBridge ()
{
    shared_ptr<Core> core;
    UserInterface * ui;
}

@end

@implementation NoteBridge


- (NoteBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_
{
    if(!(self = [super init])) return self;
    core = core_;
    ui = ui_;
    
    [self linkCoreWithUi];
    return self;
}


- (void) linkCoreWithUi
{
    // CreateNote
    [ui.noteList setOnNewNote:^
    {
        self->core->notes.createNote();
    }];
    
    // onNoteCreated
    core->notes.onNoteCreated = [self](const string & noteId)
    {
        self->core->notes.loadNote(noteId);
    };

    // OpenNote
    ui.noteList.onOpenNote = ^(NSString * _Nonnull noteId)
    {
        self->core->notes.loadNote([noteId UTF8String]);
    };

    // onNoteOpened
    core->notes.onNoteLoaded = [self](string noteId, string noteText)
    {
        [self onNoteLoaded:noteId noteText:noteText];
    };

    // onNoteChanged
    core->notes.onNoteChanged = [self](shared_ptr<NoteInfo> noteInfo)
    {
        [self onNoteChanged:noteInfo];
    };

    // ModifyNote
    [ui.editor setOnNoteModified:^(string noteId, unsigned long selectionStart, unsigned long selectionEnd, string editSequence)
    {
        self->core->notes.editNote(noteId, selectionStart, selectionEnd, editSequence);
    }];
    
    // onNoteEdited
    core->notes.onNoteEdited = [self](shared_ptr<NoteInfo> noteInfo, string noteText, unsigned long selectionStart, unsigned long selectionEnd)
    {
        string noteHtml = core->gui.convertNoteToHtml(noteText);
        [ui.editor setNote:noteInfo->noteId noteHtml:noteHtml withSelectionStart:selectionStart selectionEnd:selectionEnd];
    };

    // onLinkClicked
    ui.editor.onLinkClicked = ^(NSString * _Nonnull url)
    {
        [self clickLink:url];
    };

    // Delete note
    [ui.editor setOnDeleteNote:^(string noteId)
    {
        self->core->notes.deleteNote(noteId);
    }];
    
    [ui.noteList setOnDeleteNote:^(NSString * noteId)
    {
        self->core->notes.deleteNote([noteId UTF8String]);
    }];

    // NoteDeleted
    core->notes.onNoteDeleted = [self](string noteId)
    {
        [self onNoteDeleted:noteId];
    };
}


- (void) onNoteDeleted:(string) noteId
{
    if([ui.editor openedNote] == noteId)
    {
        [ui.editor closeNote];
    }
}


- (void) onNoteLoaded:(string)noteId_ noteText: (string)noteText
{
    // Highlight opened note in note list (if there is the note)
    auto noteId = [NSString stringWithUTF8String:noteId_.c_str()];
    [ui.noteList setOpenedNote:noteId];
    [ui.editor setEditable:YES];

    // New (or empty) note
    if(noteText.length() == 0)
        [ui.editor setFocused:YES];

    // Convert from markdown to HTML
    string noteHtml = core->gui.convertNoteToHtml(noteText);
    [ui.editor setNote:noteId_ noteHtml:noteHtml];
}


- (void) onNoteChanged:(shared_ptr<NoteInfo>) noteInfo
{
    // If the changed note is opened, load it
    if(ui.editor.openedNote == noteInfo->noteId)
        core->notes.loadNote(noteInfo->noteId);
}


- (void) clickLink:(NSString *) url
{
    NSString * sherePrefix = @"shere://";
    
    if ([url hasPrefix:sherePrefix]) {
        // Open a note
        NSString * shereUrl = [url substringFromIndex:[sherePrefix length]];
        string noteId = [shereUrl UTF8String];
        core->notes.loadNote(noteId);
    } else {
        // Open a website
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:url]];
    }
}


@end
