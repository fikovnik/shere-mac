//
//  TaskBridge.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-03-27
//

#import "TaskBridge.h"

@interface TaskBridge ()
{
    shared_ptr<Core> core;
    UserInterface * ui;
}

@end

@implementation TaskBridge


- (TaskBridge *) initWithCore:(shared_ptr<Core>)core_ userInterface:(UserInterface *)ui_
{
    if(!(self = [super init])) return self;
    core = core_;
    ui = ui_;
    
    [self linkCoreWithUi];
    return self;
}


- (void) linkCoreWithUi
{
    id self_ = self;

    ui.sidebar.onShowTasksClicked = ^
    {
        self->core->tasks.loadTasks();
    };
   
    core->tasks.onTasksLoaded = [self_](const string & tasksText)
    {
       [self_ openTasks:tasksText];
    };
}


//
// Actions
//


- (void) openTasks:(const string &) tasksText
{
    string tasksText_ = tasksText;
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->ui.noteList deselectAllNotes];
        
        // Convert from markdown to HTML
        string noteHtml = self->core->gui.convertNoteToHtml(tasksText_);
        [self->ui.editor setNote:"tasks" noteHtml:noteHtml];
        [self->ui.editor setEditable:NO];
    });
}

@end

