//
//  UserInterface.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

/**
 * Class that contains user interface objects:
 * app window, sidebar, ...
 */
#import <Foundation/Foundation.h>
#import "Window.h"
#import "Sidebar.h"
#import "NoteList.h"
#import "Editor.h"


/**
 * Container class that contains all UI components
 */
@interface UserInterface : NSObject

@property (readonly) Window * window;
@property (readonly) Sidebar * sidebar;
@property (readonly) NoteList * noteList;
@property (readonly) Editor * editor;

@end
