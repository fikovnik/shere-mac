//
//  UINoteHeader.m
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-12
//

#import "UINoteHeader.h"

@implementation UINoteHeader


- (id) initWithId: (NSString *) noteId title: (NSString *) title
{
    if(!(self = [super init])) return self;
    _noteId = noteId;
    _title = title;
    return self;
};


- (BOOL) isEqual:(id)object
{
    return [object isKindOfClass:[self class]] && [_noteId isEqual:[object noteId]];
}


- (NSUInteger) hash
{
    return [_noteId hash];
}


@end
