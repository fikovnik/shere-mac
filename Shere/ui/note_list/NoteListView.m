//
//  NoteListView.m
//  Shere
//
//  Created by Marek Foltýn on 29.05.18.
//  Copyright © 2018 Marek Foltýn. All rights reserved.
//

#import "NoteListView.h"

@implementation NoteListView

- (void)rightMouseDown:(NSEvent *)theEvent
{
    NSPoint globalLocation = [theEvent locationInWindow];
    NSPoint localLocation = [self convertPoint:globalLocation fromView:nil];
    NSInteger clickedRow = [self rowAtPoint:localLocation];
    
    if(clickedRow < 0)
        return;
    else
        [super rightMouseDown:theEvent];
}

@end
