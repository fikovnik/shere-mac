//
//  UINoteHeader.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-12
//

#import <Foundation/Foundation.h>

/**
 * Class that wraps note header class from Shere Core
 */
@interface UINoteHeader : NSObject

/**
 * Note identifier
 */
@property NSString * noteId;

/**
 * Note title
 */
@property NSString * title;

/**
 * Construction
 */
- (id) initWithId: (NSString *) noteId title: (NSString *) title;

@end
