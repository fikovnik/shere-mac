//
//  NoteListView.h
//  Shere
//
//  Created by Marek Foltýn on 29.05.18.
//  Copyright © 2018 Marek Foltýn. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NoteListView : NSTableView

@end
