//
//  NoteList.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import <Cocoa/Cocoa.h>

/**
 * UI component that shows list of notes
 */
@interface NoteList : NSObject<NSTableViewDelegate, NSMenuDelegate>


//
// Actions
//


/**
 *  // Set array of UINoteHeader *
 */
- (void) setNotes:(NSArray * _Nonnull) noteList;

/**
 * Select a note in the list. If the note is not in the list, select nothing
 */
- (void) setOpenedNote:(NSString * _Nonnull) noteId;

/**
 * Toggle fullscreen mode that ajdust the UI for fullscreen experience
 */
- (void) setFullscreenMode:(BOOL) enabled;

/**
 * Select no note in the list
 */
- (void) deselectAllNotes;


//
// Callbacks
// http://fuckingblocksyntax.com/
//


/**
 * A note in the list was clicked
 */
@property (nonatomic, copy, nullable) void (^onOpenNote) (NSString * _Nonnull noteId);

/**
 * The new note button was clicked
 */
@property (nonatomic, copy, nullable) void (^onNewNote) (void);

/**
 * Delete note was clicked
 */
@property (nonatomic, copy, nullable) void (^onDeleteNote) (NSString * _Nonnull noteId);


//
// NSTableViewDelegate methods
//

- (void)tableViewSelectionDidChange:(NSNotification *_Nullable)notification;

//
// Other
//


/**
 * Get cocoa root view of the sidebar
 */
- (NSView *_Nonnull) view;


@end
