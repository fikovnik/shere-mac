//
//  NoteList.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import <Cocoa/Cocoa.h>
#import "NoteList.h"
#import "UINoteHeader.h"
#import "NoteListView.h"

@interface NoteList () <NSTableViewDataSource>
{
    NSString * openedNote;
    NSMutableArray * noteHeaderArray;
    
    NSColor * backgroundColor;
    NSView * mainView;
    NSStackView * actionStack;
    NSLayoutConstraint * actionStackTopConstraint;
    NSLayoutConstraint * actionStackTopFullscreenConstraint;
    NSButton * btnNew;
    NSScrollView * scrollView;
    NoteListView * noteTable;
    // Keep Blocks allocated (setTarget uses weak reference)
    id newNoteBlock;
}

//
// NSTAbleViewDataSource methods
//

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView;
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row;

@end

@implementation NoteList


//
// Actions
//


- (void) setFullscreenMode:(BOOL) enabled
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->actionStackTopConstraint setActive:!enabled];
        [self->actionStackTopFullscreenConstraint setActive:enabled];
    });
}


- (void) setNotes:(NSArray *) noteList
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->noteHeaderArray removeAllObjects];
        [self->noteHeaderArray addObjectsFromArray:noteList];
        [self sortHeaders];
        [self->noteTable reloadData];
        [self refreshOpenedNote];
    });
}


- (void) setOpenedNote:(NSString *) noteId
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        self->openedNote = noteId;
        [self refreshOpenedNote];
    });
}


- (void) deselectAllNotes
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->noteTable deselectAll:nil];
        self->openedNote = @"";
    });
}


//
// Other
//


- (NoteList *_Nullable) init
{
    if (!(self = [super init])) return self;
    noteHeaderArray = [[NSMutableArray alloc] init];
    [self initGui];
    return self;
}


- (void) initGui
{
    //backgroundColor = [NSColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    
    [self initMainView];
    [self initActionStack];
    [self initNoteList];
}


- (void) initMainView
{
    mainView = [[NSView alloc] init];
    [mainView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [mainView setWantsLayer:YES];
    [mainView.layer setBackgroundColor:backgroundColor.CGColor];
    [[[mainView widthAnchor] constraintGreaterThanOrEqualToConstant:180] setActive:YES];
}


- (void) initActionStack
{
    id self_ = self;
    newNoteBlock = ^{ if([self_ onNewNote]) [self_ onNewNote]();};

    actionStack = [[NSStackView alloc] initWithFrame:NSMakeRect(0, 0, 180, 29.5)];
    [actionStack setTranslatesAutoresizingMaskIntoConstraints:NO];
    [actionStack setOrientation:NSUserInterfaceLayoutOrientationHorizontal];
    [actionStack setDistribution:NSStackViewDistributionGravityAreas];
    [mainView addSubview:actionStack];
    
    [[[actionStack leftAnchor] constraintEqualToAnchor:[mainView leftAnchor]] setActive:YES];
    [[[actionStack rightAnchor] constraintEqualToAnchor:[mainView rightAnchor]] setActive:YES];
    actionStackTopConstraint = [[actionStack topAnchor] constraintEqualToAnchor:[mainView topAnchor]];
    [actionStackTopConstraint setActive:YES];
    actionStackTopFullscreenConstraint = [[actionStack topAnchor] constraintEqualToAnchor:[mainView topAnchor] constant:4];
    [actionStackTopFullscreenConstraint setActive:NO];
    [[[actionStack heightAnchor] constraintEqualToConstant:30] setActive:YES];
    
    btnNew = [[NSButton alloc] init];
    [btnNew setBordered:NO];
    [btnNew setImage: [NSImage imageNamed:NSImageNameAddTemplate]];
    [btnNew setToolTip:@"New Note (⌘N)"];
    [btnNew setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationHorizontal];
    [btnNew setTarget:newNoteBlock];
    [btnNew setAction:@selector(invoke)];
    [actionStack addView:btnNew inGravity:NSStackViewGravityLeading];
    [[[btnNew widthAnchor] constraintEqualToConstant:29.5] setActive:YES]; // TODO: figure out a better way how to preserve default size (11x11)
    [[[btnNew heightAnchor] constraintEqualToConstant:29.5] setActive:YES];
}


- (void) initNoteList
{
    auto self_ = self;

    // Note list
    noteTable = [[NoteListView alloc] init];
    [noteTable setHeaderView:nil];
    [noteTable setTranslatesAutoresizingMaskIntoConstraints:NO];
    [noteTable setDataSource:self];
    [noteTable setDelegate:self];
    [noteTable setBackgroundColor:backgroundColor];
    [noteTable setFocusRingType:NSFocusRingTypeNone];
    [noteTable setIntercellSpacing:NSMakeSize(16, 10)];
    NSTableColumn * column = [[NSTableColumn alloc] initWithIdentifier: @"heading"];
    [column setEditable: NO];
    [noteTable addTableColumn: column];
    // action and target is handled via delegate method
    
    // Note contextual menu
    NSMenu * noteMenu = [[NSMenu alloc] initWithTitle:@"Contextual Menu"];
    [noteMenu setDelegate:self_];
    [noteTable setMenu:noteMenu];
    
    NSMenuItem * copyNoteLink = [[NSMenuItem alloc] initWithTitle:@"Copy Link to Note" action:@selector(invoke) keyEquivalent:@""];
    [copyNoteLink setTarget:self_];
    [copyNoteLink setAction:@selector(copyNoteLink)];
    [noteMenu addItem:copyNoteLink];
    
    // Separator in between
    NSMenuItem * separatorMenuItem = [NSMenuItem separatorItem];
    [noteMenu addItem:separatorMenuItem];
 
    NSMenuItem * deleteNote = [[NSMenuItem alloc] initWithTitle:@"Delete" action:@selector(invoke) keyEquivalent:@""];
    [deleteNote setTarget:self_];
    [deleteNote setAction:@selector(deleteNote:)];
    [noteMenu addItem:deleteNote];
 
    scrollView = [[NSScrollView alloc] init];
    [scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scrollView setContentHuggingPriority:NSLayoutPriorityDefaultLow-4 forOrientation:NSLayoutConstraintOrientationVertical];
    [scrollView setDrawsBackground:NO];
    [scrollView setHasVerticalScroller:YES];
    [scrollView setDocumentView:noteTable];
    [mainView addSubview:scrollView];
    
    [[[scrollView leftAnchor] constraintEqualToAnchor:[mainView leftAnchor]] setActive:YES];
    [[[scrollView rightAnchor] constraintEqualToAnchor:[mainView rightAnchor]] setActive:YES];
    [[[scrollView topAnchor] constraintEqualToAnchor:[actionStack bottomAnchor]] setActive:YES];
    [[[scrollView bottomAnchor] constraintEqualToAnchor:[mainView bottomAnchor]] setActive:YES];
    
    [[[noteTable leftAnchor] constraintEqualToAnchor:[scrollView leftAnchor]] setActive:YES];
    [[[noteTable rightAnchor] constraintEqualToAnchor:[scrollView rightAnchor]] setActive:YES];
    [[[noteTable topAnchor] constraintEqualToAnchor:[scrollView topAnchor]] setActive:YES];
}


- (void) openSelectedNote
{
    NSInteger selectedRow = [noteTable selectedRow];
    
    // Note: it is possible to select row behind the existing indices
    // (noteTable.allowsEmptySelection is true to allow deselect all)
    if(selectedRow >= [noteHeaderArray count])
        return;
    
    UINoteHeader * header = [noteHeaderArray objectAtIndex:selectedRow];
    if(_onOpenNote) _onOpenNote(header.noteId);
}


- (void) refreshOpenedNote
{
    for(int i=0; i < [noteHeaderArray count]; ++i)
    {
        UINoteHeader * header = [noteHeaderArray objectAtIndex:i];
        if([[header noteId] isEqualToString:openedNote])
        {
            [noteTable selectRowIndexes:[NSIndexSet indexSetWithIndex:i] byExtendingSelection:NO];
            return;
        }
    }
    
    // The note is not in list
    [noteTable deselectAll:nil];
}


- (NSView *_Nonnull) view
{
    return mainView;
}


- (void) sortHeaders
{
    [noteHeaderArray sortUsingComparator: ^(UINoteHeader * h1, UINoteHeader * h2)
    {
        return [h1.title localizedCaseInsensitiveCompare:h2.title];
    }];
}

- (void) deleteNote:(id)sender
{
    auto row = [noteTable clickedRow];
    UINoteHeader * noteHeader = [noteHeaderArray objectAtIndex:row];
    if(_onDeleteNote) _onDeleteNote(noteHeader.noteId);
}


- (void) copyNoteLink
{
    auto row = [noteTable clickedRow];
    UINoteHeader * noteHeader = [noteHeaderArray objectAtIndex:row];
    NSString * link = [NSString stringWithFormat:@"[%@](shere://%@)", noteHeader.title, noteHeader.noteId];
    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] setString:link forType:NSPasteboardTypeString];
}


//
// NSTableViewDelegate methods
//


- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    [self openSelectedNote];
}

//
// NSMenuDelegate methods for table view
//

- (void)menuNeedsUpdate:(NSMenu *)menu {
    NSInteger clickedRow = [noteTable clickedRow];
    return;
}


//
// NSTAbleViewDataSource methods
//


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [noteHeaderArray count];
}


- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString * title = [(UINoteHeader *)[noteHeaderArray objectAtIndex:row] title];
    title = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if([title isEqualToString:@""]) title = @"(No title)";
    return title;
}


@end
