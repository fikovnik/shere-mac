//
//  Sidebar.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <string>

using std::string;

/**
 * UI component representing sidebar that contain tag hierarchy
 */
@interface Sidebar : NSObject<NSOutlineViewDelegate, NSSearchFieldDelegate>


//
// Actions
//


/**
 * Toggle fullscreen mode that ajdust the UI for fullscreen experience
 */
- (void) setFullscreenModeEnabled:(BOOL) enabled;

/**
 * Reload tag tree data. Call when the tag tree changes.
 */
- (void) reloadTags;

/**
 * Toggle highlight state of the button 'show untagged notes'
 */
- (void) setUntaggedHighlighted:(BOOL) highlighted;

/**
 * Check if untagged notes are selected
 */
- (BOOL) isUntaggedHighlighted;

/**
 * Remove selection highlight from the tag hierarchy
 */
- (void) deselectAllTags;

/**
 * Set tag outline view data source
 */
- (void) setDataSource:(id<NSOutlineViewDataSource> _Nullable) dataSource;

/**
 * Focus search field
 */
 - (void) focusSearchField;

/**
 * Get selected tag
 */
- (string) selectedTag;

//
// Callbacks
// http://fuckingblocksyntax.com/
//


/**
 * User selected a tag
 */
@property (nonatomic, copy, nullable) void (^onTagSelected) (NSString * _Nonnull tagPath);

/**
 * User clicked the 'show untagged notes' button
 */
@property (nonatomic, copy, nullable) void (^onShowUntaggedClicked) ();

/**
 * User clicked the 'show tasks' button
 */
@property (nonatomic, copy, nullable) void (^onShowTasksClicked) ();

/**
 * When the search has finished
 */
@property (nonatomic, copy, nullable) void (^onSearchEnd) ();

/**
 * When the search string has occured
 */
@property (nonatomic, copy, nullable) void (^onSearch) (NSString * _Nonnull searchQuery);

//
// NSOutlineViewDelegate methods
//


- (void)outlineViewSelectionDidChange:(NSNotification *_Nullable)notification;


//
// Other
//


/**
 * Get root view of the sidebar
 */
- (NSView *_Nonnull) view;

/**
 * Get the outline view associated with the sidebar
 */
- (NSOutlineView * _Nonnull) outlineView;

@end
