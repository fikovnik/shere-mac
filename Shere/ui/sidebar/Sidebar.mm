//
//  Sidebar.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import "Sidebar.h"
#import "NSOutlineViewExtensions.m"

// https://stackoverflow.com/a/25573950
#define RGBA(r,g,b,a) [NSColor colorWithCalibratedRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f]

@interface Sidebar ()
{
    NSView * mainView;
    NSStackView * bottomStack;
    NSTextView * searchHelp;
    NSSearchField * txtSearch;
    NSLayoutConstraint * txtSearchTopConstraint;
    NSButton * btnTask;
    NSButton * btnUntagged;
    NSScrollView * scrollView;
    NSOutlineView * tagOutline;
    
    string selectedTagPath;
}

@end

@implementation Sidebar


//
// Actions
//


- (void) reloadTags
{
    dispatch_async(dispatch_get_main_queue(),^
    {
        [self->tagOutline reloadData];
    });
}


- (void) setFullscreenModeEnabled:(BOOL) enabled
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        if(enabled)
            [self->txtSearchTopConstraint setConstant:8];
        else
            [self->txtSearchTopConstraint setConstant:32];
    });
}


- (void) setUntaggedHighlighted:(BOOL) highlighted
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        if(highlighted)
        {
            [self->btnUntagged setWantsLayer:YES];
            [self->btnUntagged.layer setBackgroundColor:RGBA(20, 80, 208, 255).CGColor];
            [self->btnUntagged setImage:[NSImage imageNamed:@"no_tag-selected"]];
        } else {
            [self->btnUntagged setWantsLayer:NO];
            [self->btnUntagged.layer setBackgroundColor:[NSColor clearColor].CGColor];
            [self->btnUntagged setImage:[NSImage imageNamed:@"no_tag"]];
        }
    });
}

- (BOOL) isUntaggedHighlighted
{
    // Prevent deadlock due to dispatch_sync()
    if([NSThread isMainThread])
        return [self->btnUntagged wantsLayer];
    
    // Send pointer to Block instead of string value
    BOOL __block untaggedHighlighted = NO;
    dispatch_sync(dispatch_get_main_queue(),
    ^{
        untaggedHighlighted = ([self->btnUntagged wantsLayer]);
    });
    
    return untaggedHighlighted;
}


- (void) deselectAllTags
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self->tagOutline deselectAll:nil];
    });
}

- (void) focusSearchField
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [[self->txtSearch window] makeFirstResponder:self->txtSearch];
    });
}


- (void) setDataSource:(id<NSOutlineViewDataSource>) dataSource
{
    [tagOutline setDataSource:dataSource];
}


- (string) selectedTag
{
    // Prevent deadlock due to dispatch_sync()
    if([NSThread isMainThread])
        return selectedTagPath;
    
    // Send pointer to Block instead of string value
    string __block tag;
    dispatch_sync(dispatch_get_main_queue(),
    ^{
        tag = self->selectedTagPath;
    });
    
    return tag;
}


//
// NSOutlineViewDelegate methods
//


- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
    [self selectTag];
}


//
// NSSearchFieldDelegate methods
//


- (BOOL)control:(NSControl *)control textShouldBeginEditing:(NSText *)fieldEditor
{
    [self setSearchModeEnabled:YES];
    return YES;
}


- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    if([[txtSearch stringValue] length] == 0)
        [self setSearchModeEnabled:NO];
    return YES;
}




//
// Others
//


- (Sidebar *) init
{
    if(!(self = [super init])) return self;
    [self initGui];
    return self;
}


- (void) initGui
{
    // Main stack - stacks everything in sidebar
    // Placed in a split view -> constrained automatically to fill the whole split
    mainView = [[NSView alloc] init];
    [mainView setWantsLayer:YES];
    //NSColor * backgroundColor = [NSColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
    //[mainView.layer setBackgroundColor:backgroundColor.CGColor];
    [[[mainView widthAnchor] constraintGreaterThanOrEqualToConstant:180] setActive:YES];
    
    // Search field
    txtSearch = [[NSSearchField alloc] init];
    [txtSearch setTranslatesAutoresizingMaskIntoConstraints:NO];
    [txtSearch setFocusRingType:NSFocusRingTypeNone];
    [txtSearch setPlaceholderString:@"Search Notes"];
    [txtSearch setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationVertical];
    [txtSearch setSendsSearchStringImmediately:YES];
    [txtSearch setDelegate:self];
    [txtSearch setTarget:self];
    [txtSearch setAction:@selector(onSearchQueryChanged)];
    [mainView addSubview:txtSearch];
    txtSearchTopConstraint = [txtSearch.topAnchor constraintEqualToAnchor:mainView.topAnchor constant:32];
    [txtSearchTopConstraint setActive:YES];
    [[txtSearch.leftAnchor constraintEqualToAnchor:mainView.leftAnchor constant:8] setActive:YES];
    [[txtSearch.rightAnchor constraintEqualToAnchor:mainView.rightAnchor constant:-8] setActive:YES];
    
    // Search help
    searchHelp = [[NSTextView alloc] initWithFrame:NSMakeRect(0, 0, 180, 30)];
    [searchHelp setBackgroundColor:[NSColor clearColor]];
    [searchHelp setTextColor:[NSColor blackColor]];
    [searchHelp setSelectable:NO];
    [searchHelp setRichText:YES];
    [searchHelp setTextContainerInset: NSMakeSize(8, 8)];
    NSString * helpPath = [[NSBundle mainBundle] pathForResource:@"help" ofType:@"html"];
    NSString * helpFileString = [[NSString alloc] initWithContentsOfFile:helpPath encoding:NSUTF8StringEncoding error:nil];
    // https://stackoverflow.com/questions/17682559/nstextview-and-nsattributedstring
    NSAttributedString * help = [[NSAttributedString alloc] initWithData:[helpFileString dataUsingEncoding:NSUTF8StringEncoding]
                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                            NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                            documentAttributes:nil error:nil];
    [[searchHelp textStorage] appendAttributedString:help];
    
    // Stack view at the bottom
    bottomStack = [[NSStackView alloc] initWithFrame:NSMakeRect(0, 0, 180, 30)];
    [bottomStack setTranslatesAutoresizingMaskIntoConstraints:NO];
    [bottomStack setSpacing:0];
    [bottomStack setHuggingPriority:NSLayoutPriorityRequired-2 forOrientation:NSLayoutConstraintOrientationVertical];
    [mainView addSubview:bottomStack];
    [[bottomStack.bottomAnchor constraintEqualToAnchor:mainView.bottomAnchor] setActive:YES];
    [[bottomStack.leftAnchor constraintEqualToAnchor:mainView.leftAnchor] setActive:YES];
    [[bottomStack.rightAnchor constraintEqualToAnchor:mainView.rightAnchor] setActive:YES];

    // Show untagged notes
    btnTask = [[NSButton alloc] init];
    NSImage * imgTask = [NSImage imageNamed:@"task"];
    [btnTask setBordered:NO];
    [btnTask setImage:imgTask];
    [btnTask setToolTip:@"Show tasks"];
    [btnTask setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationHorizontal];
    [btnTask setTarget:self];
    [btnTask setAction:@selector(btnTaskClick)];
    [bottomStack addView:btnTask inGravity:NSStackViewGravityTrailing];
    [[[btnTask widthAnchor] constraintEqualToConstant:30] setActive:YES]; // TODO: figure out a better way how to preserve default size (11x11)
    [[[btnTask heightAnchor] constraintEqualToConstant:30] setActive:YES];

    // Show untagged notes
    btnUntagged = [[NSButton alloc] init];
    NSImage * imgUntagged = [NSImage imageNamed:@"no_tag"];
    [btnUntagged setBordered:NO];
    [btnUntagged setImage:imgUntagged];
    [btnUntagged setToolTip:@"Show untagged notes"];
    [btnUntagged setContentHuggingPriority:NSLayoutPriorityRequired forOrientation:NSLayoutConstraintOrientationHorizontal];
    [btnUntagged setTarget:self];
    [btnUntagged setAction:@selector(btnUntaggedClick)];
    [bottomStack addView:btnUntagged inGravity:NSStackViewGravityLeading];
    [[[btnUntagged widthAnchor] constraintEqualToConstant:30] setActive:YES]; // TODO: figure out a better way how to preserve default size (11x11)
    [[[btnUntagged heightAnchor] constraintEqualToConstant:30] setActive:YES];
    
    // Note list outline view
    tagOutline = [[NSOutlineView alloc] init];
    [tagOutline setBackgroundColor:[NSColor clearColor]];
    [tagOutline setFocusRingType:NSFocusRingTypeNone];
    [tagOutline setIntercellSpacing:NSMakeSize(16, 10)];
    [tagOutline setHeaderView:nil];
    [tagOutline setIndentationPerLevel:18];
    NSTableColumn * column = [[NSTableColumn alloc] initWithIdentifier: @"heading"];
    [column setEditable: NO];
    [tagOutline addTableColumn:column];
    [tagOutline setOutlineTableColumn: column];
    [tagOutline setDelegate:self];
    [tagOutline setTarget: self];
    // action is handled via delegate method
    [tagOutline setDoubleAction:@selector(expandActualRow)];
    
    // Tag list scroll view
    scrollView = [[NSScrollView alloc] init];
    [scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scrollView setContentHuggingPriority:NSLayoutPriorityDefaultLow-4 forOrientation:NSLayoutConstraintOrientationVertical];
    [scrollView setDrawsBackground:NO];
    [scrollView setDocumentView:tagOutline];
    [mainView addSubview:scrollView];
    [[scrollView.topAnchor constraintEqualToAnchor:txtSearch.bottomAnchor constant:8] setActive:YES];
    [[scrollView.leftAnchor constraintEqualToAnchor:mainView.leftAnchor constant:0] setActive:YES];
    [[scrollView.rightAnchor constraintEqualToAnchor:mainView.rightAnchor constant:0] setActive:YES];
    [[scrollView.bottomAnchor constraintEqualToAnchor:bottomStack.topAnchor constant:-8] setActive:YES];
    
    [self setFullscreenModeEnabled:NO];
    [self setUntaggedHighlighted:YES];
}


- (void) selectTag
{
    NSInteger row = [tagOutline selectedRow];
    NSString * tagPath = [tagOutline itemAtRow:row];
    
    if(tagPath == nil){
        [self btnUntaggedClick];
    } else {
        if(_onTagSelected)
            _onTagSelected(tagPath);
    }
}


- (void) expandActualRow
{
    auto row = [tagOutline clickedRow];
    id item = [tagOutline itemAtRow:row];
    
    if([tagOutline isItemExpanded:item])
        [tagOutline collapseItem:item];
    else
        [tagOutline expandItem:item];
}


- (void) btnUntaggedClick
{
    if(_onShowUntaggedClicked) _onShowUntaggedClicked();
}


- (void) btnTaskClick
{
    if(_onShowTasksClicked) _onShowTasksClicked();
}


- (void) onSearchQueryChanged
{
    NSString * query = [txtSearch stringValue];
    if(_onSearch)
        _onSearch(query);
}


- (void) setSearchModeEnabled:(BOOL) enabled
{
    if(enabled){
        [scrollView setDocumentView:searchHelp];
    } else {
        [scrollView setDocumentView:tagOutline];
        if(_onSearchEnd)
            _onSearchEnd();
    }

    [bottomStack setHidden:enabled];
    
}


- (NSView *) view { return mainView; }
- (NSView *) outlineView { return tagOutline; }

@end
