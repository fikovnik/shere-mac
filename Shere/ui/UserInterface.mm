//
//  UserInterface.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import "UserInterface.h"

@interface UserInterface ()

- (void) setupSplitView;
- (void) setupSidebar;
- (void) setupNoteList;
- (void) setupEditor;

@end


@implementation UserInterface


- (UserInterface *) init
{
    if (!(self = [super init])) return self;
    
    // Init GUI
    _window = [[Window alloc] init];
    _sidebar = [[Sidebar alloc] init];
    _noteList = [[NoteList alloc] init];
    _editor = [[Editor alloc] init];
    
    [self setupSidebar];
    [self setupNoteList];
    [self setupEditor];
    [self setupSplitView];
    
    [[_window.splitView window] makeFirstResponder:_noteList.view];
    
    return self;
}


- (void) setupSplitView
{
    [[_window splitView] setHoldingPriority:NSLayoutPriorityDefaultLow+2 forSubviewAtIndex:0];
    [[_window splitView] setHoldingPriority:NSLayoutPriorityDefaultLow+1 forSubviewAtIndex:1];
    [[_window splitView] setHoldingPriority:NSLayoutPriorityDefaultLow forSubviewAtIndex:2];
}


- (void) setupSidebar
{
    auto self_ = self;

    // Add into view hierarchy
    auto view = [_sidebar view];
    [[_window splitView] addSubview:view];
    
    // Link window with sidebar (fullscreen toggle changes layout a bit)
    _window.onFullscreenEnter = ^{
        [self_.sidebar setFullscreenModeEnabled:YES];
        [self_.noteList setFullscreenMode:YES];
    };
    _window.onFullscreenExit = ^{
        [self_.sidebar setFullscreenModeEnabled:NO];
        [self_.noteList setFullscreenMode:NO];
        
    };
}


- (void) setupNoteList
{
    [[_window splitView] addSubview:_noteList.view];
}


- (void) setupEditor
{
    auto self_ = self;

    // Load HTML and wait for the callback (to avoid delayed window show)
    _editor.onLoaded = ^{
        [self_.window show];
    };
    [_editor loadHTML];

    // Add into view hierarchy
    auto view = [_editor view];
    [[_window splitView] addSubview:view];
}


@end
