//
//  SplitView.m
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-07-02
//

#import "SplitView.h"

@implementation SplitView

- (CGFloat) dividerThickness
{
    return 0;
}

- (NSColor *) dividerColor
{
    return [NSColor colorWithRed:0.8627451 green:0.8627451 blue:0.8627451 alpha:1];
}

@end
