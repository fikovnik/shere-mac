//
//  Window.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

/**
 * Class representing main application window of Shere
 * Besides NSWindow it also creates root NSSplitView where all the other UI
 * components are placed.
 */
@interface Window : NSWindowController <NSWindowDelegate>


//
// Actions
//


/**
 * Show the window. If the window was not created yet, it will automatically
 * create one.
 */
- (void) show;


//
// Callbacks
//


/**
 * Window is switched to fullscreen
 */
@property (nonatomic, copy, nullable) void (^onFullscreenEnter)();

/**
 * Window is switched from fullscreen to normal
 */
@property (nonatomic, copy, nullable) void (^onFullscreenExit)();


//
// Other
//


/**
 * Get the main split view
 */
@property (readonly) NSSplitView * _Nonnull splitView;

@end
