//
//  Window.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-02
//

#import "Window.h"
#import "SplitView.h"

@interface Window ()
{
    NSWindow * window;
}

@end


@implementation Window


//
// Actions
//


- (void) show
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [self showWindow:self];
        [self->window makeMainWindow];
    });
}


//
// Other
//


- (Window *) init
{
    if (!(self = [super init])) return self;
    [self prepareWindow];
    return self;
}


- (void) createWindow
{
    // Window style flags
    NSUInteger windowStyle = (
                              // Adds titlebar (no buttons)
                              NSWindowStyleMaskTitled
                              // Exit button (others disabled)
                              | NSWindowStyleMaskClosable
                              // Fullscreen button
                              | NSWindowStyleMaskResizable
                              // Minimize button
                              | NSWindowStyleMaskMiniaturizable
                              // Blurred title bar
                              | NSWindowStyleMaskFullSizeContentView
                              );
    
    // Window bounds (x, y, width, height).
    NSRect windowRect = NSMakeRect(0, 0, 600, 600);
    //NSRect windowRect = NSMakeRect(0, 0, 1100, 700);

    // Window
    window = [[NSWindow alloc] initWithContentRect:windowRect styleMask:windowStyle backing:NSBackingStoreBuffered defer:YES];
    [window setTitlebarAppearsTransparent: YES];
    [window setDelegate:self];
    [window setTitleVisibility:NSWindowTitleHidden];
    [window setMovableByWindowBackground:YES];
    
    // Window controller:
    [self setWindow:window];
    [window center];
}


- (void) prepareWindow
{
    [self createWindow];
    
    // Create split view
    NSRect windowRect = [[window contentView] frame];
    _splitView = [[SplitView alloc] initWithFrame: windowRect];
    [_splitView setVertical:YES];
    [_splitView setTranslatesAutoresizingMaskIntoConstraints:NO]; // removing this disables window resizability
    [_splitView setDividerStyle:NSSplitViewDividerStyleThin];
    [window setContentView:_splitView];
    
    // Fill the window
    NSView * cv = [window contentView];
    [[[_splitView topAnchor] constraintEqualToAnchor: [cv topAnchor]] setActive:YES];
    [[[_splitView bottomAnchor] constraintEqualToAnchor: [cv bottomAnchor]] setActive:YES];
    [[[_splitView leadingAnchor] constraintEqualToAnchor: [cv leadingAnchor]] setActive:YES];
    [[[_splitView trailingAnchor] constraintEqualToAnchor: [cv trailingAnchor]] setActive:YES];
}


//
// NSWindowDelegate methods
//

- (void)windowWillEnterFullScreen:(NSNotification *)notification
{
    if(_onFullscreenEnter) _onFullscreenEnter();
}


- (void)windowWillExitFullScreen:(NSNotification *)notification
{
    if(_onFullscreenExit) _onFullscreenExit();
}

@end
