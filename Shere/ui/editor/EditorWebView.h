//
//  EditorWebView.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-26
//

#import <WebKit/WebKit.h>

@interface EditorWebView : WKWebView

- (void) pasteDebugText;

- (void) showAllHidable;

@end
