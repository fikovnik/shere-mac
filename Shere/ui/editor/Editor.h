//
//  Editor.h
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-14
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import <string>

using std::string;

/**
 * UI component that shows markdown content in a pretty HTML-rendered way.
 * It also allows sending edit commands (but does not edits on its own)
 */
@interface Editor : WKUserContentController


//
// Actions
//


/**
 * Explicitly load HTML content into the web view. Call this only once after
 * setting onLoaded action.
 */
- (void) loadHTML;

/**
 * Set note content.
 */
- (void) setNote:(string)noteId noteHtml:(string)noteHtml;

/**
 * Set note content and text selection.
 */
- (void) setNote:(string)noteId noteHtml:(string)noteHtml withSelectionStart:(unsigned long)selectionStart selectionEnd:(unsigned long)selectionEnd;

/**
 * (Un)Set note view focus
 */
- (void) setFocused:(bool)focused;

/**
 * remove note content, focus and disable editation.
 */
- (void) closeNote;

- (void) setEditable:(BOOL) editable;

/**
 * Get noteId of actually opened note. Thread-safe.
 */
- (string) openedNote;

//
// Callbacks
//

// http://fuckingblocksyntax.com/
@property (nonatomic, copy, nullable) void (^onLoaded) ();
@property (nonatomic, copy, nullable) void (^onNoteModified) (string noteId, unsigned long selectionStart, unsigned long selectionEnd, string editSequence);
@property (nonatomic, copy, nullable) void (^onDeleteNote) (string noteId);
@property (nonatomic, copy, nullable) void (^onLinkClicked) (NSString * _Nonnull url);
@property (nonatomic, copy, nullable) void (^onTagClicked) (NSString * _Nonnull tagPath);

//
// Others
//


/**
 * Get cocoa view of the sidebar
 */
- (NSView *_Nonnull) view;


@end
