
// List of nodes that are hidable, but for now they are visible (they are near
// or in caret selection)
var unhiddenNodes = [];

// Saved actual selection
var actualSelection;
var lastSelection;

// Check if the element has the specified class
function hasClass(elem, className)
{
    return elem.classList.contains(className);
}


// Handle click dynamically (links for example)
// https://stackoverflow.com/a/27373951
function handleDynamicClick(event)
{
    let node = event.target;
    if(hasClass(node, "tag-text") && !event.altKey) clickTag(node);
    else if(hasClass(node, "link-text") && !event.altKey) clickLink(node);
    else if(hasClass(node, "task-checkbox")) clickTask(node)
    return true;
}


// Perform a click on a tag node
function clickTag(node)
{
    let tagPath = node.innerText;
    window.webkit.messageHandlers.onTagClicked.postMessage(tagPath);

    event.preventDefault();
    event.stopPropagation();

    setSelection(lastSelection.start, lastSelection.end);
    return false;
}


// Perform a click on a link node
function clickLink(node)
{
    let url = node.getAttribute("href");
    window.webkit.messageHandlers.onLinkClicked.postMessage(url);
    
    event.preventDefault();
    event.stopPropagation();
    
    if(lastSelection != null)
        setSelection(lastSelection.start, lastSelection.end);
    return false;
}


// Perform a click on a task checkbox node
function clickTask(node)
{
    if(!isEnabled()) return;

    // Send editation
    let index = getIndexBefore(node);
    let edit = hasClass(node.parentElement, "task-done") ? "-" : "+";
    postEditSequence(index, index+1, edit);
}

// Send a notification that HTML (with JS) was loaded
function onHtmlLoaded()
{
    window.webkit.messageHandlers.htmlLoaded.postMessage(null);
}


// Log a string using NSLog inside obj-c runtime
function log(text)
{
    window.webkit.messageHandlers.log.postMessage(text);
}


// Get the editor node
function editor()
{
    return document.querySelector('#editor');
}


// Send edit event to obj-c runtime
function postEditSequence(selectionStart, selectionEnd, editSequence)
{
    var messageBody = {};
    messageBody.selectionStart = selectionStart;
    messageBody.selectionEnd = selectionEnd;
    messageBody.editSequence = editSequence;
    window.webkit.messageHandlers.postEditSequence.postMessage(messageBody);
}


// Enable / disable content editability (default: disabled)
function setEnabled(enabled)
{
    if(enabled) {
        editor().setAttribute("contenteditable", "plaintext-only");
        document.body.classList.remove("disabled");
    } else {
        editor().setAttribute("contenteditable", "false");
        document.body.classList.add("disabled");
        refreshHidable();
    }
}


// Check if the editor is editable
function isEnabled()
{
    return !document.body.classList.contains("disabled");
}


// Toggle editor focus (note that webview's cocoa focus is handled separately,
// this handles only editor's node focus)
function setFocused(focused)
{
    if(focused) {
        editor().focus();
    } else {
        editor().blur();
    }
}


// Set html content of editor
function setText(text, refreshSelection=false)
{
    let selection = getPlainSelection();
    editor().innerHTML = text;
    
    if(refreshSelection && selection != null)
        setSelection(selection.start, selection.end);
}


// Check if the node is element (not text) node
function isElementNode(node)
{
    return node.nodeType === 1;
}


// Check if the node is text (not element) node
function isTextNode(node)
{
    // https://developer.mozilla.org/en-US/docs/Web/API/Node
    return node.nodeType === 3;
}


// Check if the node is <br>
function isBR(node)
{
    return node.nodeName == "BR";
}


// Determine node content length
function nodeLength(node)
{
    if(isTextNode(node))
    {
        return node.length;
    }
    else if(isBR(node))
    {
        return 1;
    }
    else
    { // element-non-BR node (must have data-length attribute!)
        return Number(node.getAttribute("data-length"));
    }
}


// Compute html node and offset of markdown-index. This function is sort of
// inversion of getIndexBefore(node).
function getAnchorAndOffset(selectionIndex)
{
    let indexFound = false;
    let remainingIndex = selectionIndex;
    let anchor = editor();
    let offset = 0;
    
    // Corner case: empty note (skip traversal and return default)
    if(editor().firstChild != null)
    {
        // Note DOM tree traversal
        let actualNode = editor().firstChild; // if the note is not empty, there must be first child
        while(true)
        {
            let length = nodeLength(actualNode);
            
            // In "ahoj" there are length+1=5 possible indices. If selection should
            // be on the end of the node and there is a sibling, put the selection
            // on the beginning of the sibling in order to work with empty paragraphs.
            if(remainingIndex > length && actualNode.nextSibling != null)
            {
                // Move on to the next node
                remainingIndex -= length;
                actualNode = actualNode.nextSibling;
                continue;
            }
            
            if(actualNode.firstChild != null)
            {
                // Staying in this node, let's find exact subnode
                actualNode = actualNode.firstChild; // https://www.w3schools.com/jsref/prop_node_firstchild.asp
                
                // No text node nor newline
                if( !isTextNode(actualNode) || !isBR(actualNode))
                {
                    continue;
                }
            }
            
            // No more children / siblings. Let's find offset
            anchor = actualNode;
            offset = remainingIndex;
            break;
        }
    }
    
    if(offset > 0)
    {
        // Corner case - this fixes bad caret position.
        if(isBR(anchor)){
            let nextLeaf = nextLeafOf(anchor);
            if(nextLeaf != null)
                anchor = nextLeaf;
            offset = 0;
        } else {
            // When external change of the note occurs, the new text can be shorter than
            // actual selection. So check the maximum.
            let maxOffset = nodeLength(anchor);
            if(offset > maxOffset)
                offset = maxOffset;
        }
    }
    
    // https://stackoverflow.com/a/2917186
    return {
        anchor: anchor,
        offset: offset
    }
}


// Convert markdown-plaintext selection to html selection (nodes and offsets)
function setSelection(selectionStart, selectionEnd)
{
    // Update hidden nodes before setting selection. This workaround fixes bad
    // caret placement.
    let selection = {
        start: selectionStart,
        end: selectionEnd
    };
    refreshHidable(selection);
    
    let windowSelection = window.getSelection();
    if(windowSelection.rangeCount == 0) return; // no selection available...
    let range = windowSelection.getRangeAt(0); // we assume exactly one range

    let start = getAnchorAndOffset(selectionStart);
    let end = getAnchorAndOffset(selectionEnd);
    
    range.setStart(start.anchor, start.offset);
    range.setEnd(end.anchor, end.offset);
    
    windowSelection.removeAllRanges();
    windowSelection.addRange(range);
    
    // Scroll that the selection start is always visible
    // http://roysharon.com/blog/37
    // TODO: dont scroll if the selection is visible
    // TODO: if the selection is below the visible frame, scroll that the caret
    //       is at bottom (not top) of the view frame.
    //if(isTextNode) start.anchor.parentElement.scrollIntoView();
    //else start.anchor.scrollIntoView();
}


// Compute markdown-width of foregoing tokens
function getIndexBefore(node)
{
    let offset = 0;
    let element = node;
    
    if(isTextNode(node))
    {
        element = node.parentElement;
    }
    
    let previousSibling = element.previousSibling;
    let parent = element.parentElement;
    
    while(parent != null && parent.nodeName != "BODY")
    {
        while(previousSibling != null)
        {
            offset += Number(previousSibling.dataset.length);
            previousSibling = previousSibling.previousSibling;
        }
        previousSibling = parent.previousSibling;
        parent = parent.parentElement;
    }
    
    return offset;
}


// Compute markdown-plaintext selection indices
function getPlainSelection()
{
    let windowSelection = window.getSelection();
    if(windowSelection.rangeCount == 0) return null; // nothing to select...
    
    let range = windowSelection.getRangeAt(0); // we assume exactly one range
    
    let anchorNode = windowSelection.anchorNode;
    let focusNode = windowSelection.focusNode;
    
    let anchorOffset = windowSelection.anchorOffset;
    anchorOffset += getIndexBefore(anchorNode);
    let focusOffset = windowSelection.focusOffset;
    focusOffset += getIndexBefore(focusNode);
    
    let startOffset = Math.min(anchorOffset, focusOffset);
    let endOffset = Math.max(anchorOffset, focusOffset);
    
    let selectionLog = "Selection: " + startOffset + " -> " + endOffset;
    log(selectionLog);
    
    let selection = {};
    selection.start = startOffset;
    selection.end = endOffset;
    return selection;
}


// Get previous element-node leaf (in-order tree traversal back)
function previousLeafOf(node)
{
    if(node == editor()) return null;
    let actualNode = node;

    // Get the leftmost neighbour (or parent)
    while(true)
    {
        if(actualNode.previousSibling != null) {
            actualNode = actualNode.previousSibling;
            break;
        } else if(actualNode.parentNode != null && actualNode.parentNode != editor()) {
            actualNode = actualNode.parentNode;
            continue;
        }
        
        // No leftmost neighbour leaf
        return null;
    }
    
    // Get the rightmost child
    while(true)
    {
        if(actualNode.lastChild != null){
            actualNode = actualNode.lastChild;
            continue;
        }
        return actualNode;
    }
}


// Get next node leaf (in-order tree traversal forward)
function nextLeafOf(node)
{
    if(node == editor()) return null;
    let actualNode = node;

    // Get the rightmost neighbour (or parent)
    while(true)
    {
        if(actualNode.nextSibling != null) {
            actualNode = actualNode.nextSibling;
            break;
        } else if(actualNode.parentNode != null && actualNode.parentNode != editor()) {
            actualNode = actualNode.parentNode;
            continue;
        }
        
        // No rightmost neighbour leaf
        return null;
    }
    
    // Get the leftmost child
    while(true)
    {
        if(actualNode.firstChild != null){
            actualNode = actualNode.firstChild;
            continue;
        }
        return actualNode;
    }
}


// Check if the node is hidable and hidden and eventually show
// If the node is in hidable-group, show all group members
function unhideNode(node)
{
    if(node == null) return;
    if(isTextNode(node)) node = node.parentElement;
    if(hasClass(node, "hidable-hidden"))
    {
        node.classList.remove("hidable-hidden");
        unhiddenNodes.push(node);
                
        // Show all paired nodes
        if(node.hasAttribute("data-hidable-group"))
        {
            let parent = node.parentElement;
            if(parent == null) return;
            
            for(let child of parent.children)
            {
                if(isElementNode(child) &&
                   child.hasAttribute("data-hidable-group") &&
                   hasClass(child, "hidable-hidden"))
                {
                    child.classList.remove("hidable-hidden");
                    unhiddenNodes.push(child);
                }
            }
        }
    }
}


// Hide all hidable tokens that are visible
function hideAllHidable()
{
    for(let node of unhiddenNodes)
            node.classList.add("hidable-hidden");
    unhiddenNodes = [];
}


// Refresh hidable tokens - show tokens around selection
function refreshHidable(selection)
{
    hideAllHidable();

    if(!isEnabled()) return;

    let nodes = [
        getAnchorAndOffset(selection.start).anchor,
        getAnchorAndOffset(selection.end).anchor
    ];
    
    if(nodes[0] === nodes[1]) nodes.pop(); // Selection size 0

    // Show hidden tokens around
    for(let node of nodes)
    {
        if(isTextNode(node)) node = node.parentElement;
    
        let neighbourNodes = [
            previousLeafOf(node),
            node,
            nextLeafOf(node)
        ];
        
        for(let neighbour of neighbourNodes)
        {
            unhideNode(neighbour);
        }
    }
}


// Dev-function
// Refresh hidable tokens - show tokens around selection
function showAllHidable()
{
    let hidables = document.querySelectorAll('[data-hidable], [data-hidable-group]');
    for(let node of hidables)
    {
        unhideNode(node)
    }
}


// This stupid workaround fixes bad caret rendering when jumping from visible text
// to visible text but surrounded with hidable text. The hidable text will show up,
// but the caret will move after next blink. And we don't want this.
function refreshSelection()
{
    let selection = getPlainSelection();
    if(selection == null) return;
    lastSelection = actualSelection;
    actualSelection = selection;
    refreshHidable(selection);
    setSelection(selection.start, selection.end);
}


// Set the editor's width to fill the entire screen width
function updateEditorSize()
{
    let minPadding = 52;
    let width = window.innerWidth;
    let height = window.innerHeight;
    let e = editor();
    let padding = (width - 576)/2;
    if(padding < minPadding) // TODO: extract constant
    {
        padding = minPadding;
    }
    
    let marginRight = String(minPadding) + "px";
    let paddingRight = String(padding - minPadding) + "px";
    padding = String(padding) + "px";
    if(e==null) return;
    
    e.style.paddingLeft = padding;
    e.style.marginRight = marginRight; // margin to avoid text cursor
    e.style.paddingRight = paddingRight;
    
    //e.style.height = height;
    
    refreshSelection();
    onHtmlLoaded();
}


// Initialize editor
function main()
{
    setEnabled(false);
    updateEditorSize();
    
    //
    // Set event listeners
    //
    
    // Dynamic click listener
    document.addEventListener('click', handleDynamicClick);
    
    editor().addEventListener('keypress', function(event)
    {
        if(event.key == "Escape") return true;
        if(event.metaKey) return true; // ignore key presses with meta keys and propagate them forward
    
        let selection = getPlainSelection();
        let editSequence = String.fromCharCode(event.keyCode);
        log("editSequence: " + editSequence);
        postEditSequence(selection.start, selection.end, editSequence);
        
        // prevent the default behaviour of return key pressed
        event.preventDefault();
        event.stopPropagation();
        return false;
    });

    editor().addEventListener('keydown', function(event)
    {
        refreshSelection();
        
        if(event.key === "Delete" || event.key === "Backspace")
        {
            let selection = getPlainSelection();
            let keyCode = (event.key === "Delete") ? 127 : 8;
            let editSequence = String.fromCharCode(keyCode);
            
            log("editSequence: (delete or remove)");
            postEditSequence(selection.start, selection.end, editSequence);
        
            // prevent the default behaviour of the key
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        
        if(event.key == "Enter")
        {
            // Insert LF (\n)
            let editSequence = String.fromCharCode(10); // LF
            let selection = getPlainSelection();
            postEditSequence(selection.start, selection.end, editSequence);
            
            // prevent the default behaviour of the key
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        
        return true;
    });
    
    editor().addEventListener('keyup', function(event)
    {
        refreshSelection();
        return true;
    });
    
    editor().addEventListener('mousedown', function(event)
    {
        //refreshSelection();
        return true;
    });
    
    editor().addEventListener('mouseup', function(event)
    {
        refreshSelection();
        return true;
    });
    
    editor().addEventListener('scroll', function(event)
    {
        refreshSelection();
        return true;
    });
    
    // Catch clipboard
    editor().onpaste = function(event)
    {
        let text = event.clipboardData.getData("text")
        if(text != null && text.length > 0)
        {
            let selection = getPlainSelection();
            postEditSequence(selection.start, selection.end, text);
        }
        
        event.preventDefault();
        event.stopPropagation();
    };
    
    editor().oncut = function(event)
    {
        let selection = getPlainSelection();
        let deleteChar = String.fromCharCode(8);
        postEditSequence(selection.start, selection.end, deleteChar);
        // Do not stop event -> cutted text must propagate to the clipboard
    };
    
    document.body.addEventListener("click", function()
    {
        if(editor().getAttribute("contenteditable") != "false")
        {
            editor().focus();
        }
        return true;
    });
    
    window.addEventListener("resize", function()
    {
        updateEditorSize();
        return true;
    });
    
    window.onblur = hideAllHidable;
}

window.onload = main;
