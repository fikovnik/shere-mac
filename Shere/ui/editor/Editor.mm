//
//  Editor.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-14
//


#import "Editor.h"
#import "EditorWebView.h"

@interface Editor ()  <WKScriptMessageHandler>
{
    NSView * mainContainer;
    EditorWebView * webView;
    NSStackView * buttonStack;
    NSButton * btnFormat;
    NSButton * btnInfo;
    NSButton * btnDelete;
    
    string openedNote;
    
    id deleteBlock; // keep allocated (setTarget uses weak reference)
    id infoBlock;
}
@end


@implementation Editor


//
// Actions
//


- (void) loadHTML
{
    auto content = [self loadEditorHtml];
    [webView loadHTMLString:content baseURL: nil];
}


- (void) setNote:(string)noteId noteHtml:(string)noteHtml
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        self->openedNote = noteId;
        [self setNoteHtmlContent:noteHtml];
    });
}


- (void) setNote:(string)noteId noteHtml:(string)noteHtml withSelectionStart:(unsigned long)selectionStart selectionEnd:(unsigned long)selectionEnd
{
    // Already runs async - set selection after setting the content
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self setNoteHtmlContent:noteHtml selectionStart:selectionStart selectionEnd:selectionEnd];
    });
}


- (void) setFocused:(bool)focused
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        if(focused) {
            [[self->webView window] makeFirstResponder:self->webView];
        } else {
            [[self->webView window] makeFirstResponder:nil];
        }
    });
    
    
    dispatch_sync(dispatch_get_main_queue(),
    ^{
        [self setFocusedInJavaScript:focused];
    });
}


- (void) closeNote
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        self->openedNote = "";
        [self setNoteHtmlContent:""];
        [self setEnabled:false];
    });
}


- (void) setEditable:(BOOL) editable
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self setEnabled:editable];
    });
}


- (string) openedNote
{
    // Prevent deadlock due to dispatch_sync()
    if([NSThread isMainThread]) return openedNote;
    
    // Send pointer to Block instead of string value
    auto noteId = std::make_shared<string>("");
    dispatch_sync(dispatch_get_main_queue(),
    ^{
        *noteId = self->openedNote;
    });
    
    return *noteId;
}


//
// Others
//


- (NSView *) view
{
    return mainContainer;
}


//
// Internals
//


- (Editor *) init
{
    if(!(self = [super init])) return self;
    [self initGui];
    return self;
}


- (void) initGui
{
    id self_ = self;
    auto frame = NSMakeRect(0, 0, 800, 400);
    
    // Main stack - stacks everything in editor
    // Placed in a split view -> constrained automatically to fill the whole split
    mainContainer = [[NSView alloc] initWithFrame:frame];
    //[mainContainer setOrientation:NSUserInterfaceLayoutOrientationHorizontal];
    //[mainContainer setSpacing:0];
    [[[mainContainer widthAnchor] constraintGreaterThanOrEqualToConstant:300] setActive:YES];
    
    // Important: if you want to open WebKit web inspector on the web view,
    // you have to set the web view width as small as possible. This will force
    // the inspector to be opened in a separate window instead of inside of the
    // main window view hierarchy which causes the inspector not working.

    // Editor web view
    webView = [[EditorWebView alloc] initWithFrame:frame];
    [webView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[webView configuration] setUserContentController:self];
    [[[webView configuration] preferences] setValue:@YES forKey:@"developerExtrasEnabled"]; //https://stackoverflow.com/a/29069844
    [mainContainer addSubview:webView];
    [[[webView topAnchor] constraintEqualToAnchor:[mainContainer topAnchor]] setActive:YES];
    [[[webView leftAnchor] constraintEqualToAnchor:[mainContainer leftAnchor]] setActive:YES];
    [[[webView widthAnchor] constraintEqualToAnchor:[mainContainer widthAnchor]] setActive:YES];
    [[[webView heightAnchor] constraintEqualToAnchor:[mainContainer heightAnchor]] setActive:YES];
    
    
    // Stack view for editor buttons on the right side
    buttonStack = [[NSStackView alloc] initWithFrame:NSMakeRect(0, 0, 30, 50)];
    [buttonStack setTranslatesAutoresizingMaskIntoConstraints:NO];
    [buttonStack setOrientation:NSUserInterfaceLayoutOrientationVertical];
    [buttonStack setAlignment:NSLayoutAttributeCenterX];
    [buttonStack setEdgeInsets:NSEdgeInsetsMake(14, 0, 14, 0)]; // padding
    [buttonStack setHidden:YES];
    [buttonStack setSpacing:16];

    [mainContainer addSubview:buttonStack];
    [[[buttonStack topAnchor] constraintEqualToAnchor:[mainContainer topAnchor]] setActive:YES];
    [[[buttonStack rightAnchor] constraintEqualToAnchor:[mainContainer rightAnchor] constant:-14] setActive:YES];
    [[[buttonStack heightAnchor] constraintEqualToAnchor:[mainContainer heightAnchor]] setActive:YES];
    
    auto width = 24;
    auto height = width;
    auto imgSize = NSMakeSize(18,18);
    
    // Format button
    NSImage * imgFormat = [NSImage imageNamed:@"pen"];
    [imgFormat setSize:imgSize];
    btnFormat = [[NSButton alloc] initWithFrame:NSMakeRect(0, 0, width, height)];
    [btnFormat setBordered:NO];
    [btnFormat setWantsLayer:YES];
    [btnFormat setImage: imgFormat];
    [btnFormat setToolTip:@"Note styles"];
    [buttonStack addView:btnFormat inGravity:NSStackViewGravityTop];
    [[[btnFormat widthAnchor] constraintEqualToConstant:width] setActive:YES];
    [[[btnFormat heightAnchor] constraintEqualToConstant:height] setActive:YES];
    //formatBLock = ^{ if([self_ onShowInfo]) [self_ onShowInfo]([self_ openedNote]);};
    //[btnFormat setTarget:deleteBlock];
    //[btnFormat setAction:@selector(invoke)];
    [btnFormat setEnabled:NO];
    
    // Info button
    NSImage * imgInfo = [NSImage imageNamed:@"info"];
    [imgInfo setSize:imgSize];
    btnInfo = [[NSButton alloc] initWithFrame:NSMakeRect(0, 0, width, height)];
    [btnInfo setBordered:NO];
    [btnInfo setWantsLayer:YES];
    [btnInfo setImage: imgInfo];
    [btnInfo setToolTip:@"Show info"];
    [buttonStack addView:btnInfo inGravity:NSStackViewGravityTop];
    [[[btnInfo widthAnchor] constraintEqualToConstant:width] setActive:YES];
    [[[btnInfo heightAnchor] constraintEqualToConstant:height] setActive:YES];
    [btnInfo setEnabled:NO];
    //infoBLock = ^{ if([self_ onShowInfo]) [self_ onShowInfo]([self_ openedNote]);};
    //[btnInfo setTarget:deleteBlock];
    //[btnInfo setAction:@selector(invoke)];

    // Delete button
    NSImage * imgTrash = [NSImage imageNamed:@"garbage"];
    [imgTrash setSize:imgSize];
    btnDelete = [[NSButton alloc] initWithFrame:NSMakeRect(0, 0, width, height)];
    [btnDelete setBordered:NO];
    [btnDelete setWantsLayer:YES];
    [btnDelete setImage: imgTrash];
    [btnDelete setToolTip:@"Delete Note"];
    [buttonStack addView: btnDelete inGravity:NSStackViewGravityLeading];
    [[[btnDelete widthAnchor] constraintEqualToConstant:width] setActive:YES];
    [[[btnDelete heightAnchor] constraintEqualToConstant:height] setActive:YES];
    // https://stackoverflow.com/a/7132007
    deleteBlock = ^{ if([self_ onDeleteNote]) [self_ onDeleteNote]([self_ openedNote]);};
    [btnDelete setTarget:deleteBlock];
    [btnDelete setAction:@selector(invoke)];
}


// Prepare HTML, CSS and JavaScript content to inject into webview
- (NSString *) loadEditorHtml
{
    NSMutableString * content = [[NSMutableString alloc] init];
    
    // Load CSS
    auto cssPath = [[NSBundle mainBundle] pathForResource:@"editor" ofType:@"css"];
    auto css = [[NSString alloc] initWithContentsOfFile:cssPath encoding:NSUTF8StringEncoding error:nil];
    [content appendString:@"<style>"];
    [content appendString:css];
    [content appendString:@"</style>"];
    
    // Load JavaScript
    auto jsPath = [[NSBundle mainBundle] pathForResource:@"editor" ofType:@"js"];
    auto js = [[NSString alloc] initWithContentsOfFile:jsPath encoding:NSUTF8StringEncoding error:nil];
    [content appendString:@"<script>"];
    [content appendString:js];
    [content appendString:@"</script>"];
    
    // Load HTML
    auto htmlPath = [[NSBundle mainBundle] pathForResource:@"editor" ofType:@"html"];
    auto html = [[NSString alloc] initWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    [content appendString:html];
    
    // Register messages from javascript
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"log"];
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"postEditSequence"];
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"htmlLoaded"];
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"onTagClicked"];
    [webView.configuration.userContentController addScriptMessageHandler:self name:@"onLinkClicked"];
    
    return content;
}


// Receive messages from javascript to objective-c
- (void) userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message
{
    if([[message name] isEqualToString:@"log"])
    {
        NSLog(@"%@", [message body]);
    }
    
    if([[message name] isEqualToString:@"postEditSequence"])
    {
        id body = [message body];
        NSString * editSequence = [body objectForKey:@"editSequence"];
        NSNumber * selectionStart = [body objectForKey:@"selectionStart"];
        NSNumber * selectionEnd = [body objectForKey:@"selectionEnd"];
    
        if(self.onNoteModified)
        {
            self.onNoteModified(openedNote, [selectionStart unsignedLongValue], [selectionEnd unsignedLongValue], [editSequence UTF8String]);
        }
    }
    
    if([[message name] isEqualToString:@"htmlLoaded"] && _onLoaded)
    {
        _onLoaded();
    }
    
    if([[message name] isEqualToString:@"onTagClicked"] && _onTagClicked)
    {
        NSString * tagPath = [message body];
        _onTagClicked(tagPath);
    };
    
    if([[message name] isEqualToString:@"onLinkClicked"] && _onTagClicked)
    {
        NSString * url = [message body];
        _onLinkClicked(url);
    };
}


// Show HTML in web view
- (void) setNoteHtmlContent:(string)noteHtml
{
    auto content = [NSString stringWithUTF8String:noteHtml.c_str()]; // Passing directly noteHtml.c_str() to stringWithFormat causes bad UTF8 characters showing
    auto script = [NSString stringWithFormat:@"setText(\"%@\", true);", content];
    [webView evaluateJavaScript:script completionHandler:nil];
}


// Show HTML and set selection in web view
- (void) setNoteHtmlContent:(string)noteHtml selectionStart:(unsigned long)selectionStart selectionEnd:(unsigned long) selectionEnd
{
    auto noteHtmlCStr = noteHtml.c_str();
    auto content = [NSString stringWithUTF8String:noteHtmlCStr]; // Passing directly noteHtml.c_str() to stringWithFormat causes bad UTF8 characters showing
    auto script = [NSString stringWithFormat:@"setText(\"%@\"); setSelection(%ld, %ld);", content, selectionStart, selectionEnd];
    [webView evaluateJavaScript:script completionHandler:nil];
}


// Set the contenteditable area focused
- (void) setFocusedInJavaScript:(bool)focused
{
    if(focused) {
        [webView evaluateJavaScript:@"setFocused(true);" completionHandler:nil];
    } else {
        [webView evaluateJavaScript:@"setFocused(false);" completionHandler:nil];
    }
}


// Allow / disable note to be edited
- (void) setEnabled:(bool)enabled
{
    if(enabled)
    {
        [webView evaluateJavaScript:@"setEnabled(true);" completionHandler:nil];
        [buttonStack setHidden:NO];
    } else {
        [webView evaluateJavaScript:@"setEnabled(false);" completionHandler:nil];
        [buttonStack setHidden:YES];
    }
}


// Set note selection
- (void) setSelectionStart:(unsigned long)selectionStart selectionEnd:(unsigned long)selectionEnd
{
    auto js = [NSString stringWithFormat:@"setSelection(%ld, %ld)", selectionStart, selectionEnd];
    [webView evaluateJavaScript:js completionHandler:nil];
}


@end
