//
//  EditorWebView.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2018-02-26
//

#import "EditorWebView.h"

@implementation EditorWebView

- (void) pasteDebugText
{
    auto textPath = [[NSBundle mainBundle] pathForResource:@"debugText" ofType:@"html"];
    auto text = [[NSString alloc] initWithContentsOfFile:textPath encoding:NSUTF8StringEncoding error:nil];
    text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    auto js = [NSString stringWithFormat:@"editor().innerHTML = \"%@\";", text];
    js = [js stringByAppendingString:[NSString stringWithFormat:@"raw().value = \"%@\";", text]];
    [self evaluateJavaScript:js completionHandler:nil];
}


- (void) showAllHidable
{
    auto js = [NSString stringWithFormat:@"showAllHidable();"];
    [self evaluateJavaScript:js completionHandler:nil];
}

@end
