//
//  ShereApp.mm
//  author: Marek Foltýn (marek@foltynovi.cz)
//  created: 2017-11-16
//

#import "ShereApp.h"
#import <Shere/Core.h>
#import "ui/UserInterface.h"
#import "bridge/NoteBridge.h"
#import "bridge/TaskBridge.h"
#import "bridge/tags/TagBridge.h"
#import "bridge/main_menu/MainMenu.h"
#import <memory>

// Interface extension
@interface ShereApp ()
{
    // UI widgets, main window
    UserInterface * ui;
    
    // Shere Core: logic, networking, file I/O
    std::shared_ptr<Shere::Core> core;
    
    // Core <-> UI bridges
    NoteBridge * noteBridge;
    TagBridge * tagBridge;
    TaskBridge * taskBridge;
    MainMenu * mainMenu;
}
@end
@implementation ShereApp


/**
 * NSApp-related initialization, custom init is in
 * applicationDidFinishLaunching.
 */
- (ShereApp *) init
{
    if (!(self = [super init])) return self;
    
    // Create a shared app instance.
    // This will initialize the global variable
    // 'NSApp' with the application instance.
    [NSApplication sharedApplication];
    
    // ShereApp will handle events fromm NSApp
    [NSApp setDelegate:self];

    return self;
}


/**
 * Launch Shere app in main().
 */
- (void) run
{
    [NSApp run];
}


/**
 * Get path to Shere folder. The path will be stored in file:
 * ~/.config/Shere/corePath.txt
 *
 * If the file does not exist, ~/Shere will be used
 * TODO: show prompt on first start, where to store notes!
 */
- (NSString *) getCorePath
{
    //NSError * _Nullable __autoreleasing * _Nullable error;
    //NSString * corePath = [NSString stringWithContentsOfFile:@"~/.config/Shere/corePath.txt" encoding:NSUTF8StringEncoding error:error];
    //NSLog(@"%@", corePath);
    return @"/Users/marek/Dropbox/Shere";
}


/**
 * Real entry point to the Shere app. Prepares two main components (UI and Core)
 * and shows main window
 */
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString * corePath = [self getCorePath];
    core = std::make_shared<Shere::Core>([corePath UTF8String]);
    ui = [[UserInterface alloc] init];
    
    noteBridge = [[NoteBridge alloc] initWithCore:core userInterface:ui];
    tagBridge = [[TagBridge alloc] initWithCore:core userInterface:ui];
    taskBridge = [[TaskBridge alloc] initWithCore:core userInterface:ui];
    mainMenu = [[MainMenu alloc] initWithCore:core userInterface:ui];
    
    // Start core
    core->runAsync();
    core->notes.reindexTagTree();
}


/**
 * Shutdown all components before the program terminates.
 */
- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    core->system.shutdown();
}


/**
 * If the all windows were closed using the red corner button and clicked on Dock, reopen the window
 */
- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)windowsVisible
{
    if(!windowsVisible)
    {
        [[ui window] show];
    }
    return YES;
}


@end
