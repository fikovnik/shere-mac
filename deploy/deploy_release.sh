#!/bin/bash

#
# Shere app deploy script
# This script must be run from the Shere repository root.
# It builds, packages and uploads all necessary files for deploying a new version.
#

# https://sipb.mit.edu/doc/safe-shell/
set -e
set -o pipefail

# Increase app version in Info.plist
#VERSION=`date +%y.%-m.%-d`
VERSION=`date +%y.%-m.%-d.%-H.%-M`
echo "version: $VERSION" | mustache - ./Shere/Info.plist > ./Shere/Info.plist_new
rm ./Shere/Info.plist
mv ./Shere/Info.plist_new ./Shere/Info.plist

# Build Shere.app and package into DMG
xcodebuild -target "Shere" -configuration Release
cd build/Release
rm -rf Shere.app.dSYM
ln -s /Applications/ Applications
hdiutil create -fs HFS+ -srcfolder ./ -volname "Shere" "Shere.dmg"
cd ../..

# Sign the archive
echo "$PRIVATE_KEY" >> dsa_priv.pem
SIGNATURE=`openssl dgst -sha1 -binary < build/Release/Shere.dmg | \
  openssl dgst -sha1 -sign dsa_priv.pem | \
  openssl enc -base64 | tail -1`

# Generate appcast.xml
PUBLISH_DATE=`LC_ALL=us_US.utf8 date "+%-d. %B %Y"`
LENGTH=`wc -c < build/Release/Shere.dmg | xargs` # xargs removes whitespaces
APPCAST_URL="http://shere.foltynovi.cz/appcast.xml"
echo "version: $VERSION" > appcast.yml
echo "publish_date: $PUBLISH_DATE" >> appcast.yml
echo "dmg_url: http://shere.foltynovi.cz/Shere.dmg" >> appcast.yml
echo "length: $LENGTH" >> appcast.yml
echo "signature: $SIGNATURE" >> appcast.yml
echo "appcast_url: $APPCAST_URL" >> appcast.yml
mustache appcast.yml deploy/appcast.mustache > appcast.xml

# Prepare everything for lftp 
mkdir website
mv build/Release/Shere.dmg website/Shere.dmg
mv appcast.xml website/appcast.xml

# Update website (appcast.xml and Shere.dmg)
lftp -c "set ftp:ssl-allow no; open -u $USERNAME,$PASSWORD $HOST; mirror -Rev ./website/ ./www/subdom/shere/"

# Clean
rm appcast.yml
rm -rf website
rm dsa_priv.pem
