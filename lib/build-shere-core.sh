if [ -z "$SRCROOT" ] 
then
    echo "SRCROOT is not defined!"
    exit 1
fi

echo "Building shere-core:"

BUILD_FOLDER="$SRCROOT/lib/shere-core/build"
mkdir -p $BUILD_FOLDER
cd "$BUILD_FOLDER"
cmake ..
make efsw
make shere-core -j
